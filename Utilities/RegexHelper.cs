﻿using System;
using System.Text.RegularExpressions;

namespace Hangman.Utilities
{
    static public class RegexHelper
    {

        static bool _invalid = false;

        static public bool IsValidString(string str)
        {
            _invalid = false;
            if (String.IsNullOrEmpty(str))
                return false;

            if (_invalid)
                return false;

            // Return true if str is in valid string format.
            try
            {
                return Regex.IsMatch(str,
                      @"[a-zA-Z]+",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}

