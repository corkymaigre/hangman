﻿using Hangman.View;
using Hangman.ViewModel;
using System.Windows;

namespace Hangman
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            new MainWindow()
            {
                DataContext = new MainWindowViewModel()
            }.Show();
        }
    }
}
