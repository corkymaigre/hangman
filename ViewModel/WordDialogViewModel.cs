﻿using Hangman.Utilities;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Hangman.ViewModel
{
    class WordDialogViewModel : ViewModelBase
    {
        RelayCommand<object> _okButtonCommand;


        private string _word;
        public string Word
        {
            get { return _word; }
            set
            {
                if (value != _word)
                {
                    _word = value;
                    NotifyPropertyChanged("Word");
                }
            }
        }

        // Ok Button Command
        public ICommand OkButtonCommand
        {
            get
            {
                if (_okButtonCommand == null)
                {
                    _okButtonCommand = new RelayCommand<object>(param => this.OkButtonCommandExecute(), param => this.OkButtonCommandCanExecute);
                }
                return _okButtonCommand;
            }
        }

        void OkButtonCommandExecute()
        {
            var currentWindow = Application.Current.Windows.Cast<Window>().SingleOrDefault(x => x.IsActive);
            currentWindow.Close();
        }

        bool OkButtonCommandCanExecute
        {
            get
            {
                return RegexHelper.IsValidString(Word);
            }
        }

        
    }
}
